---
title: Map Types
template: main-repo.html
---

Google Map provides several types of tiles:

   * Normal (Streets)
   * Satellite Images
   * Hybrid of Streets/Satellite
   * Terrain
   * No visible map

By adding a dropdown list at the top of the screen, the user can select from these types.

We create a new file to host the map-type selector. We're using the experimental `ExposedDropdownMenuBox`, so we need to {{ find("070-experimental-api", "declare we're ok") }} with using experimental API.

!!! caution

    Using experimental APIs can get you access to cool new features, but that API _is not yet stable_. If you use these in your application, there's a chance the API will change and you'll have to rewrite parts of your application. Use with care (if at all)!

The standard unidirectional data flow and externalized state patterns of Composable functions is again visible, as we need to {{ find("070-expanded-state-local", "declare expansion state") }} and manage it in the {{ find("070-dropdown", "call to `ExposedDropdownMenuBox`") }}. The expansion state is not needed outside of `MapTypeSelector` so we keep it defined locally.

We're using a {{ find("070-text-field", "read-only `TextField`") }} to display the current map type. This gives us the nice little label at the top while showing the current value in a larger font. Note the use of the `trailingIcon` parameter to add the expansion indicator for the drop-down list.

Setting the up the {{ find("070-menu", "expansion menu") }} should start to look similar to other controls. We tell it if it's `expanded` or not, and it tells us if the user wanted to change the expansion state. Inside, we loop through the possible values of `MapType` (it's an enumeration), creating each drop-down item.

When the user clicks an item, we tell the caller which `MapType` was clicked and close the drop-down menu.

Back in our `GoogleMapDisplay`, we keep track of the `MapType`. We need to share that `MapType` between the `GoogleMap` and the `MapTypeSelector`; {{ find("070-current-map-type", "defining it here") }} makes it available to both.

The `GoogleMap` uses a `MapProperties` state holder to track the map type (among other options). We'll initialize it with the "normal" (streets-only) map type, and {{ find("070-pass-properties", "pass it to the `GoogleMap`") }}.

For our layout, we'll {{ find("070-column", "wrap") }} the `GoogleMap` and `MapTypeSelector` in a `Column`. The `MapTypeSelector` stays a fixed size, while we set the `GoogleMap` {{ find("070-map-weight", "weight") }} to fill the remaining height (and tell it to fill the available width).

The resulting application now looks like this (when "Normal" map type is selected)

![Normal Map Type](screenshots/map-type-1.png){ width=400 }

and this (when "Satellite" map type is selected)

![Satellite Map Type](screenshots/map-type-2.png){ width=400 }

## All code changes
