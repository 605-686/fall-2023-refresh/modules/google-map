---
title: Dragging a Marker
template: main-repo.html
---

Sometimes the location isn't as accurate as we'd like. For example, if you're driving in New York City, GPS signals can bounce off the tall buildings making it appear you're in a different location (this is called the "canyon effect"). Or perhaps the user selected "approximate" location.

If we allow the user to drag the marker, they can position it more accurately to where the car is located.

The first thing we need to do is tell Google Map that the icon should be {{ find("130-draggable", "draggable") }}. The user can then long-press on the marker to activate drag mode and move it to another location. The MarkerState is updated with the new location.

!!! note

    When dragging a marker, it pops up to clear your finger (so you can see it). If you're using an emulator with a mouse, this may seem odd, but think about visibility of the maker when you're using a finger on a real device.

We need to watch for changes to the marker position and report it {{ find("130-event", "via onMoveCar") }}. The Google Maps Compose API doesn't directly expose an event function on `MarkerState`, so we have to be a little more clever.

'MarkerState' is a Jetpack Compose state holder. Its properties are delegated to Compose `State` (using `by mutableStateOf(...)`). From the outside, they look like normal properties.

The cool trick is that the getter and setter for these properties inform the snapshot system of who is reading the state and when the state changes. 

We can ask the snapshot system to tell up about changes using {{ find("130-snapshot-flow", "`snapshotFlow`") }}. The gist of this function is

   1. `snapshotFlow` creates a new `Flow` to report changes to the value of its lambda's value.
   2. An observer is set up to be notified whenever a new snapshot is taken. This can happen for any Compose state change, but is often applied to a group of state changes.
   3. Whenever a new snapshot is taken, the lambda passed to `snapshotFlow` is evaluated to get a value.
   4. The value is compared to the lambda's previous value (if any). If it's changed, the new value is emitted to the created `Flow`

There are two properties that we're interested in here:

   * `dragState` - whether the icon is being started dragging, being actively dragged, or finished dragging.
   * `position` - the current position of the marker.

When the `MarkerState` is initially set up, its is assigned a `position` of (0.0, 0.0). The `dragState` will be "END", meaning the marker has a position, but we do *not* want to treat that as a location. We watch the `dragState` to see when the marker is being dragged. Once we've seen it dragged, we know that the next "END" represents a `position` that we should care about. We pass that position to `onCarMoved` so the view model can be informed to update the data store with the new position.

After implementing this, when we press the star button to remember the car's location, the value is persisted to the datastore. When we quit the application and later return to it, the car location is loaded and displayed.

## All code changes
