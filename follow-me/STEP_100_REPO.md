---
title: Add Top Bar
template: main-repo.html
---

Now we'll add a top bar that provides action buttons to

   * Go to the current location
   * Remember the car is parked at the current location
   * Navigate using the Google Map application from the current position to the car (in walk mode)
   * Forget the car location

We {{ find("100-top-bar", "Define the top bar") }} with the above actions calling event parameters. 

These actions use icons from the {{ find("100-icons", "Extended Icon Set") }}, so we add its dependency. In the next step, we'll pull out the icons that we want to keep and remove the dependency to avoid creating a huge application file.

Set up a `Scaffold` that will move the camera to the current location, and placeholders for the functions we're not implementing yet. 

!!! note

    Kotlin's `TODO()` function throws an exception. Much nicer than having a TODO comment that just does nothing silently. In this case, if we tap any of the buttons that call `TODO()`, the application will crash.

Our application now looks like

![Top Bar Actions](screenshots/actions.png){ width=400 }

If we pan away from the current location and tap the location button on the Top Bar, Google Maps will animate back to the current location over one second.

## All code changes
