---
title: Manage Car Location
template: main-repo.html
---

We need to keep track of our car across application runs. To do this, we'll use a Preferences Datastore to store the latitude and longitude of the car.

The Datastore API exposes the data from the preference as a Kotlin `Flow`; whenever the preference is updated, the new value is emitted. We'll expose the car location to the UI from the view model, allowing new values to be collected. The preferences data store is a simple file that stores values. This is useful when a database would be overkill for storing small amounts of data. The datastore is typically used to hold simple application state across application runs.

We start by {{ find("120-add-datastore-deps", "Adding the Datastore Dependency") }}. This allows us to {{ find("120-add-car-location", "store and load") }} the car's location.

   * `carLatLng` is a `Flow` created by mapping the data store's `Flow` into a `LatLng` instance. If either latitude or longitude are missing, a `null` is emitted.
   * `clearCarLocation` removes the latitude and longitude from the data store.
   * `setCarLocation` stores the current location's latitude and longitude in datastore (or removes them if there is no current location)

When `clearCarLocation` or `setCarLocation` change the latitude/longitude in the data store, new data is emitted to the data store `Flow`, which will trigger the `carLatLng` to emit a new `LatLng` or null.   
   
The state in the `GoogleMapDisplay` is similar to the current location setup.

## All code changes
