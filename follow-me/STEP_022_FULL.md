---
title: Find Your Fingerprint
---

Before we can set up the key, we need to get some information from your application.

Android applications are digitally signed to uniquely identify their owners, using a self-signed certificate. When you install Android Studio, it creates a debug certificate for you, which you will _only_ use for debugging your application before deployment. Every time we've run an application, it's been signed using the debug key.

We want to restrict the API key we're creating to *only* your application id and signing key combination. For now, we'll use the debug key, but if you plan to sign with a key for releasing the application, you'll need to add the application id and release key combination.

We need to pieces of information

* application id - this is the unique identifier for your application. (You should typically start this with a reversed domain-name. For example, I own the "androidbyexample.com" domain, so I use "com.androidbyexample" as the prefix of all my application ids for the course samples.)

* SHA-1 fingerprint of your debug certification

For the application id, take a look at your `/app/build.gradle` file. You'll find the `applicationId` inside the `defaultConfig` block.

```kotlin
android {
    ...
    defaultConfig {
        applicationId = "com.androidbyexample.googlemap"
    }
}
```

Copy the application id somewhere. (I usually open a plain text file in Visual Studio Code to as a scratch pad for things like this.)

The SHA-1 fingerprint is easy to obtain. Go to the terminal at the bottom of Android Studio and type

=== "Windows"

    ```
    gradlew signingReport
    ```

=== "Linux/Mac"

    ```
    ./gradlew signingReport
    ```

Or if you open the Gradle view on the right edge of Android Studio, you can click the little elephant icon and type "signingReport" after "gradle" and hit enter.

The report will show a block like

```
Variant: debug
Config: debug
Store: C:\Users\scott\.android\debug.keystore
Alias: AndroidDebugKey
MD5: xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx
SHA1: xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx
SHA-256: xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx
Valid until: Saturday, May 25, 2052
----------
```

Copy the fingerprint after `SHA-1` and save with your application id.