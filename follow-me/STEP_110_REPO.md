---
title: Copy Icons
template: main-repo.html
---

To keep our application size and build-time reasonable, we never want to include the full extended icon set. We'll copy the icons that we don't have.

We'll do this the same way we did for the Movie example.

## All code changes
