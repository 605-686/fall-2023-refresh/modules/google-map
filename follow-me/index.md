---
title: Google Maps
---

Some applications become much more useful by displaying a Map. You might be displaying a location or area, or letting the user explore. The map could be central to your application's functionality, or a nice, optional extra. The map may or may not display the user's current location.

!!! note

    User privacy is a huge concern for your application. PII includes any information that can identify a user, such as name, address, email, phone number (among many others). Photos, contacts and files can also be sources of identifying information. Whenever this type of data is accessed, you _must_ ask the user if it's ok for the application to use it. Providers of such information are usually protected by Android Permissions that are marked as "dangerous" to indicate that they have the potential to expose PII or other private information. Device features such as location, phone and biometrics could also be used to expose PII or other personal information.

    Dangerous permissions must be requested of the user at run time. We'll see this in action when we request the user's current location.

    Note that when you publish your application to the Google Play Store, you're required to provide a Privacy Policy stating what data you use and how you use it. Regardless of where you publish your application, many states and countries have laws regarding how PII can be used and what types of information must appear in a Privacy Policy. There are many web sites that can assist in generation of a Privacy Policy (I have not researched any in detail and cannot recommend specific generators; I recommend you do some research before choosing one to ensure it generates a propery policy, and/or consult with a lawyer who knows privacy law.)

The map itself doesn't use any PII, but using Location Services to gather the user's current location does.

Setting up Google Maps requires:

   1. Add Google Maps Dependencies
   2. Set up a project in the Google Developer's Console
   3. Enable the Maps API in that project (to access map tiles and other mapping information such as POIs and address search)
   4. Create an API key to use in your application
   5. Add the key to your project 

Let's start by creating an empty project.

Example Source: [https://gitlab.com/605-686/fall-2023-refresh/modules/google-map](https://gitlab.com/605-686/fall-2023-refresh/modules/google-map)

