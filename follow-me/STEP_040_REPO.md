---
title: Add a Marker
template: main-repo.html
---

A map by itself is useful for exploring, but what if you want to tell the user where something is?

That's where markers come in, and where the `GoogleMap` content parameter becomes useful.

Our `GoogleMap` call is going to get more complex over the next several steps. By 
{{ find("040-new-map-composable", "Creating a new @Composable") }} to hold the map, we can set up parameters
and {{ find("040-factor-map", "Simplify the Caller") }}.

Our new @Composable takes a `LatLng` for the place to display the marker, a `String` description of the marker, and a `Modifier`. It's a great habit to pass `Modifiers` into your Composable functions; this allows the caller more control over how the called Composable is placed. You'll want to {{ find("040-map-modifier", "Pass the Modifier") }} to the top-level Composable that your function calls, or use it as a base modifier (rather than passing `Modifier.xxx()`, you'll pass `modifier.xxx()` to start with the passed-in `Modifier`).

Now for the marker. We need to set up the data for the Marker - we use {{ find("040-marker-state-holder", "rememberMarkerState") }} to create and `remember` a `MarkerState` instance. This instance will be re-created whenever the `key` passed to it is changed. In this example, we want to change the position whenever the `LatLng` changes. `rememberMarkerState` takes a `String` as a key, so we just convert the `LatLng` to a `String`.

Once we have data, we can {{ find("040-marker", "Create the Marker") }} in the `GoogleMap` content.

Running the application is a bit disappointing; we're still centered at 0.0, 0.0. If we pan over we'll see the marker:

![We have a Marker!](screenshots/marker-1.png){ width=400 }

You can zoom by double-tapping, pinching, or tapping the "+" button to see

![Zooming in](screenshots/marker-2.png){ width=400 }

If you tap on the marker, its description is displayed and the map centers on the marker:

![Marker Description](screenshots/marker-3.png){ width=400 }

## All code changes
