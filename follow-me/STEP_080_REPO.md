---
title: Current Location
template: main-repo.html
---

Next, let's add the user's current location. A little later we'll use this for the initial map position and to keep track of where the user parked their car.

The user's Location is determined by the "Fused Location Provider". This service uses technologies such as GPS, cell towers, and wi-fi to determine the current location. Some of these provide precise location (such as GPS), while others might only be able to approximate user location.

Because an application could send location information somewhere else (a server on the internet, for example), location is considered a "dangerous" permission, and we must ask the user if it's ok to use while the application is running.

The user has a choice: they can allow precise or approximate location information, for all runs of the application or just the current run, or deny the request. Ideally, your application should gracefully handle denied function. For our car-finder application, if the user denies current location tracking, we could, for example, allow the user to tap the location of their car on the map rather than automatically using the current location. (For this example application, we won't do that; we'll just tell the user the application cannot function without location.)

!!! note

    Requesting permissions at runtime can be a bit tricky, and support for permissions when using Jetpack Compose is still experimental (and a bit difficult to use properly). For now, we'll be requesting permissions and listening for location using the `Activity` and updating a view model with the reported locations.

Any needed permissions _must_ be declared first in the `AndroidManifest.xml`. {{ find("080-manifest-permissions", "Here") }} we declare 

   * Coarse Location (for "approximate" location)
   * Fine Location (for "precise" location)

Both of these are "dangerous" permissions and must be requested at runtime.

The Google Play Location Services allows us to set up a listener to receive location updates. We'll want to store the current location somewhere, and we'll need it when setting the car's location. The car's location will be persisted and we want to ensure it stays across configuration changes. This sounds like a job for a View Model.

We access Play Location Services by adding a {{ find("080-add-location-deps", "new dependency") }} (with definition in the version catalog)

We'll need to {{ find("080-track-current-location", "track the location") }}. We set up a `MutableStateFlow` as a `private` property in the view model. By convention, we prefix it with an underscore, indicating it's the _actual_ flow that we'll emit to. We want our view model to keep control of emitted values, so we only expose a read-only `Flow` publicly.

But our `MainActivity` will be the thing that actually talks with the Fused Location Provider; it'll need to {{ find("080-allow-activity-to-update", "update the current location") }}.

In the `MainActivity`, we {{ find("080-location-client-and-callback", "define properties") }} for the fused location provider client and the callback that we'll register to receive location updates.

We have to set up a new sequence of events to start our activity. This sequence looks like

```mermaid
graph TD
    A{Check Google Play Services}
    X[Cannot run - exit]
    X2[Cannot run - exit]
    A -->|Not Available|X
    A -->|Available|B

    B{Do we have location permission?}
    B -->|Yes|C
    B -->|No|E
    C[Start location request]
    C --> D
    D[Setup Map]

    E[Request Location Permissions]
    E --> F
    F{Were permissions granted?}
    F -->|No|G
    F -->|Yes|C
    G[Show rationale]
    G --> H
    H{Does user agree?}
    H -->|Yes|I
    H -->|No|X2
    I[Application Info Screen to change permissions - exits application]
```

We move the UI setup out of `onCreate` (we'll see it again soon), and {{ find("080-check-play-services", "check Google Play Services Availability") }}. If Play Services aren't available,  we can't run and must exit.

If Play Services are available, we {{ find("080-have-permission", "check if we have location permissions") }}. If not, we request permission. If so, we just go ahead and start our location request and set up the UI.

Requesting permissions has gotten a bit easier over the years... We {{ find("080-permission-launcher", "register a launcher") }} that will invoke the system permission requester and return whether the user granted or denied permissions. If granted, we go ahead and start the location request and set up the UI. If not granted, we go into rationale mode.

We've decided that our car finder simply cannot run without knowing the current location, so we display this rationale to the user in a dialog. (Note that this is an old-style "views" dialog, not a Jetpack Compose dialog.) The user can agree to go change the permissions (and we'll send them to the system Application Info screen to do so), or they can say "nope!" and we'll quit the application by calling `finish()` on the `Activity` (which pops it off the back stack - because we only have one `Activity` on the back stack, the application exits).

After all of the availability and permission checking, we can finally start the application in `startLocationAndMap()`. The first thing we do is {{ find("080-request-location-updates", "request location updates") }} from the fused location provider, which will use our `locationCallback` to send the results to the view model. This will emit the location, which is {{ find("080-collect-location-state", "collected") }} and made available to our composition.

We pass the location to the `GoogleMapDisplay` and {{ find("080-current-location-state", "create a `MarkerState`") }} to give us a location for a {{ find("080-marker", "new `Marker`") }}.

When we run the app, the location marker appears when it's available.

You can use the "Extended Controls" at the top of the emulator to set a fake location.

## All code changes
