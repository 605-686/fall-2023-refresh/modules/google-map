---
title: Initial Position
template: main-repo.html
---

Let's make the app more friendly by automatically moving to that marker position.

First, we {{ find("050-add-initial-position", "define a `CameraPosition`") }} to indicate where we want to jump to on the map. Think of this as a literal camera, pointing to a LatLng from a specific bearing and tilt, zoomed in/out to a certain factor.

We {{ find("050-camera-position-state", "define a `CameraPositionState`") }} similar to our `MarkerState` to hold the data for the map camera. It's position is initialized to our `defaultCameraPosition`, but as the user interacts with the map (panning, zooming, rotating, etc) this state will be updated.

Where should we define these state? Only as high in the call hierarchy as needed. Pretend that we have a `ViewModel` that needs to know when the camera moves; defining the `cameraPositionState` in `onCreate` as we're doing here gives us access to that state. If instead we defined it inside `GoogleMapView`, we would only have access to that state inside `GoogleMapView` (unless we passed an `onCameraChange` function that could be used to send out changed camera values). 

Pulling state up out of functions like this is called "State Hoisting".

We use that state {{ find("050-use-camera-position-state", "inside the `GoogleMap` call") }}.

Now when we run the application, we see the map centered on Google HQ.

## All code changes
