---
title: Lat/Lon Bounds
template: main-repo.html
---

We've seen how Google maps lets you animate panning and zooming to a location. You can also do this to a region defined by two points.

`LatLngBounds` is an immutable type that you can use to expand a region as points are added to it. Think about a real-estate application that has a list of houses for sale, and you'd like to show all of the houses on the map. You would start with a single point

```kotlin
var bounds = LatLngBounds(point1, point1)
```

and then expand it by including other points:

```kotlin
bounds = bounds.including(nextPoint)
```

Note that `including` returns a a **new** instance of `LatLngBounds`; it **does not** update the existing instance.

We can {{ find("150-jump-to-bounds", "use `LatLngBounds`") }} to include the car and current location if both are defined. Here we've expanded the auto-location logic that was based on the `currentLocation` to check if a car location had been saved. If so, we animate to a `LatLngBounds`; if not, we animate to just the current location with a zoom level.

When moving the camera position to a `LatLngBounds`, you must pass in a pixel value for how much space to leave around the edges of the map. Pixel values aren't consistent across devices, as many devices have different screen densities. We'd like to use density-independent pixels, but that means we need to convert dp to px.

Using {{ find("150-density-padding", "`LocalDensity`") }}, we can ask for the current screen density and use it access the `toPx()` function. Now we can have a consistent `48.dp` map margin on all devices.

## All code changes
