---
title: Loading Spinner
template: main-repo.html
---

Google map takes a little while to initialize. While it's loading, the user sees a blank screen, which may make them think the application isn't working.

Adding an animated progress indicator lets the user know the application isn't just hanging. We'll add an "indeterminate" (no obvious beginning/end/length) spinning indicator on top of the map for this.

We need state to keep track of whether the map has finished loading. A simple {{ find("060-marker-state-holder", "remembered `MutableState`") }} will do the trick. This drives the display of the {{ find("060-progress-spinner", "`CircularProgressIndicator`") }}.

!!! note

    Displaying or not displaying the progress indicator just takes an `if` expression. Note that we could have passed `!mapLoaded` for the `visible` parameter to `AnimatedVisibility`, but that means we'd still be looking at that Composable on every recomposition. Instead I chose to add the `if` expression around it. 

    This likely doesn't make a big difference here, but I wanted to demonstrate how normal Kotlin logic can be used to conditionally display overlays like this. Some overlays that you use (like a dialog) won't have a parameter like `visible`.

`AnimatedVisibility` is used here to fade-out the progress spinner when it's time to leave the composition. Because it's _after_ the `GoogleMap` in the `Box`, it appears on top of it. The solid background prevents any part of the map from being visible before it's completely loaded.

When the map {{ find("060-set-mapLoaded", "tells us it's loaded") }}, we set `mapLoaded`. When its value changes, recomposition is triggered and changes whether or not we include the progress indicator in the composition tree.

!!! note

    Did you notice how we changed where the passed-in `Modifier` is used? We moved it {{ find("060-map-modifier-change", "from the `GoogleMap`") }} to the {{ find("060-top-level-modifier", "to the `Box`") }}. This function declares the parent of the `GoogleMap` (the `Box`), so we have full control over its `Modifiers`.

## All code changes
