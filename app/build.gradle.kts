@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.kotlinAndroid)
    // ##START 030-add-plugin
    alias(libs.plugins.secrets)
    // ##END
}

kotlin {
    jvmToolchain(17)
}

android {
    namespace = "com.androidbyexample.googlemap"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.androidbyexample.googlemap"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    // ##START 120-add-datastore-deps
    implementation(libs.datastore.preferences)
    // ##END
    // ##START 100-icons
//    implementation(libs.icons.extended)
    // ##END
    // ##START 080-add-location-deps
    implementation(libs.location.services)
    implementation(libs.runtime.compose)
    // ##END
    // ##START 030-add-deps
    implementation(libs.maps.compose)
    implementation(libs.maps.compose.utils)
    // ##END
    implementation(libs.core.ktx)
    implementation(libs.lifecycle.runtime.ktx)
    implementation(libs.activity.compose)
    implementation(platform(libs.compose.bom))
    implementation(libs.ui)
    implementation(libs.ui.graphics)
    implementation(libs.ui.tooling.preview)
    implementation(libs.material3)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
    androidTestImplementation(platform(libs.compose.bom))
    androidTestImplementation(libs.ui.test.junit4)
    debugImplementation(libs.ui.tooling)
    debugImplementation(libs.ui.test.manifest)
}