# Google Map

NOTE - to build this application, you need to 

   * create an API key in the Google Developers Console
   * Copy the key into the local.properties file 
     
     ```
     MAPS_API_KEY=COPY_KEY_HERE
     ```

   * Update the key to only allow the application

The module at https://androidbyexample.com describes how to get a key and add it to the application.

(Alternatively you can comment out the &lt;meta-data%gt; block in the AndroidManifest.xml until after you've got your fingerprint. Note that you will not see a map until you add a valid key.)

Module Content: [https://androidbyexample.com/modules/google-map/index.html](https://androidbyexample.com/modules/google-map/index.html)
