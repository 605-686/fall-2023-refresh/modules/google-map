---
title: Google Console Project
---

Maps need to get their tile data from somewhere - in the cloud. 

Sign into [Google Developer Console](https://console.cloud.google.com/). If you don't have a Google account, you'll need to create one.

!!! note

    Map tiles are only free up to a point (at this point, $200 worth of tiles per month)... It's important that you restrict the key you'll create to your application, or someone else could use it with their application and charge you for the map tiles!!! If you're looking to create and sell a mapping application that will likely have lots of tile loads, see [Pricing](https://mapsplatform.google.com/pricing/) and set up billing for your project.

Once you're into the console, create a new project to host 

![](screenshots/console-01.png){ width=400 }

And give it a name

![](screenshots/console-02.png){ width=400 }

Enable the Maps API in your project (to access map tiles and other mapping information such as POIs and address search)

![](screenshots/console-03.png){ width=400 }

Choose "Maps SDK for Android" (if you don't see it, search for it)

![](screenshots/console-04.png){ width=400 }

And enable it

![](screenshots/console-05.png){ width=400 }

Create an API key to use in your application by going to "Credentials"

![](screenshots/console-06.png){ width=400 }

Press "Create Credentials"

![](screenshots/console-07.png){ width=400 }

Choose "API Key"

![](screenshots/console-08.png){ width=400 }

And poof! API key created! (It even says so!)

Copy the key and save it somewhere; we'll need it in the Android application later.

Click "Edit API Key" so we can restrict it to only be used by your application.

![](screenshots/console-09.png){ width=400 }

Under Application Restrictions, choose "Android Apps". This will show the "Add an Item" button. Press it.

![](screenshots/console-10.png){ width=400 }

Enter your application ID and SHA-1 fingerprint that we found in the last step, then press "Done"

![](screenshots/console-11.png){ width=400 }

Don't forget to save everything! Note that it may take a few minutes for the key to be available to your application. This is usually apparent if the map doesn't show on the screen.

![](screenshots/console-12.png){ width=400 }
