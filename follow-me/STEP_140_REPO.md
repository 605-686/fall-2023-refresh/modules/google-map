---
title: Navigating
template: main-repo.html
---

The Google Map SDK for Android's terms of use forbid using Google's navigation data for real-time navigation in your own application. If you want to show the user how to get from point A to B in real time, you need to launch the Google Maps application itself.

We {{ find("140-navigate", "launch navigation") }} using an Android `Intent`. An `Intent` describes something you would like to do, typically with a different application or system service. This intent contains a URI that represents the navigation request:

```
https://www.google.com/maps/dir/?api=1&origin=${curr.latitude},${curr.longitude}&destination=${car.latitude},${car.longitude}&travelmode=walking
```

The Google Maps application registers `IntentFilters` that watch for URIs starting with "https://www.google.com/maps". The Android platform directs this `Intent` to Google Maps, and it presents navigation options:

![Google Maps Application Navigation](screenshots/nav-1.png){ width=400 }

## All code changes
