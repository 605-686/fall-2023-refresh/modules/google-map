---
title: Jump to Current Location
template: main-repo.html
---

Let's jump to the current location when it's first available and use a custom icon instead of the default marker.

First, our custom icon. We need to get a `BitmapDescriptor`. There's no way to do that yet from Compose, so we'll define a drawable vector image resource for this.

You can create these using File->New->Vector Asset, and select an icon or pull in an SVG. This {{ find("090-icon-resource", "icon") }} is just a blue circle with a white border. This icon uses a {{ find("090-custom-color", "custom color") }}, which must be defined in a resource file, rather than through Compose for now.

We then set up {{ find("090-icon-state", "state for the icon") }} and {{ find("090-load-icon", "load the icon") }} in a coroutine once the map has loaded. 

!!! note

    The factory for the `BitmapDescriptor` sometimes isn't available until the map has loaded. It's best to wait until the map is successfully loaded before trying to load the icons.

Finally, we {{ find("090-icon-offset", "set the icon") }} on the `Marker`. 

!!! note

    The `anchor` parameter defines how the location matches up with the icon. Using an `Offset(0.5, 0.5)` means the center of the icon is pinned to the location. For the default icons, the bottom center (the pointy tip) marks the location, so we'd use `Offset(0.5, 1.0)`. 

    Mea culpa: I noticed that I accidentally copied the `Offset(0.5, 0.5)` when I first created the location marker in a previous step. It was using the default marker, so it should have used `Offset(0.5, 1.0)`.

Running the application now shows

![Custom Location Icon](screenshots/custom-icon.png){ width=400 }

{{ find("090-jump-to-location", "Jumping") }} to the current location on startup is done with a `LaunchedEffect` keyed off the `currentLocation`. The first time this `LaunchedEffect` is run, the `currentLocation` is null, and it doesn't do anything. When `currentLocation` changes, the `LaunchedEffect` is restarted, and, seeing a non-null `currentLocation`, asks the `cameraPositionState` to change via a `CameraUpdate` command. We set `initialBoundsSet` to `true` so we won't move again.

!!! note

    I'm using the name `initialBoundsSet` because later we'll be setting a bounding box based on the current location and set car location.

## All code changes
